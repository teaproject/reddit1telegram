# Reddit1Telegram

Reddit1Telegram is a Python software that will allow you to receive subreddits' feeds to a channel, group or to your private chat. This software is intended to be **lightweight, highly customisable and very easy to use**. 

You can find us on Telegram at [t.me/teaproject](https://t.me/teaproject)

# Table of contents

- [Features](#features)
- [How to use Reddit1Telegram](#how-to-use-reddit1telegram)
- [How to update your bot](#updating-the-bot)
- [Contributing](#contributing)
- [Authors](#authors) 

# Features

- Can be used in channels, groups and private chats; 
- Can be used with **multiple subreddits**; 
- Works with different feeds (new, hot, top); 
- Works with **every kind of post** (images, videos, GIFs, texts, cross-posts...); 
- **Automatically posts at specified hours and with specified frequency** (e.g. every 5 minutes); 
- Easily **configurable formatting** for posts with optional inline keyboard to original post; 
- Link to the original post is shortened. 

## Planned features 

- Markdown support for textual posts. 

# How to use Reddit1Telegram

### Step 1: download it

Download the source code using the following command: 

`$ git clone https://gitlab.com/teaproject/reddit1telegram`

If you are not sure if you do comply with Reddit1Telegram requirements please run the following command:

`$ pip3 install -r requirements.txt`

### Step 2: edit the config file

Comments in **config.yaml** explain each setting and how editing them impacts bot's activity. Feel free to edit the configuration file according to your needs. 
Please contact [@BotFather](https://t.me/botfather) to obtain your bot's token, and add it to the configuration file. 

### Step 3: run!

Simply run the main.py file! You can run it with the following command:

`$ python3 main.py`

### Updating the bot

In most cases the most simple way to update this software is to download again the main.py and the config.yaml files, then modify them as intended. 

You do not always need to modify the config.yaml. For example, when updating to a patch release (i.e. from a.b.0 to a.b.1). 

If the main.py was updated, and an updated config.yaml is required, the software will generally raise an error at start.

### Specific scenarios

#### Multiple channels

If you want to create another channel - i.e. to send a different feed to a different chat - just create a new folder, and start again from step 1. 

#### Absolute path 

In some cases you might need to specify the absolute path of the config.yaml file - e.g. using Linux cronjob -. To do so please modify in main.py the string _"config.yaml"_, first line after imports, with file's absolute path.

# Contributing 

The easiest way to contribute is by **providing bug reports**. Please provide them with as much information as possible from log files and from your configuration. This way we might, right away, understand the problem and start looking for a solution.

You can also **share your ideas** on what we could do better or functionality you will like to see. 

If you know how to use Gitlab, you can help as **updating and correcting** the documentation. 

If you know Python, you can also **send a pull request**. To do so, please follow these guidelines: 
- Your code should be readable and maintainable: use comments to say why the code is doing what it is doing, and give variables a meaningful name.  
- Ask yourself is you could make your code simpler or avoid the use of external dependencies.

Please notice that any change will be discussed, and new functionalities will be judged based on how much they are helpful, how much they complicate the code and how easy to use they are. 

## Versioning conventions

We follow a simple _Major.Minor.Patch_ convention.

A patch release is defined as any release that is part of the ordinary maintenance. 

A minor release is defined as any release that changes the software's features. 

A major release is defined as any release that significantly changes how the code works or the purpose of the software. 

0.x versions are intended as beta releases. 

Each major release is be named after a Russian writer, alphabetically. 

- 0.x versions are named after [Andreyev](https://en.wikipedia.org/wiki/Leonid_Andreyev).
- 1.x versions are named after [Bulgakov](https://en.wikipedia.org/wiki/Mikhail_Bulgakov).
- 2.x versions will be named after [Checkhov](https://en.wikipedia.org/wiki/Anton_Chekhov).
- 3.x versions will be named after [Dostoevsky](https://en.wikipedia.org/wiki/Fyodor_Dostoevsky).

# Authors

Reddit1Telegram was created (and is currently maintained) by @davideleone and is officially supported by the @teaproject 

A big thanks to our wonderful contributors: 
- @octospacc
- @onionmaster03
